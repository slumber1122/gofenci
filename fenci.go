package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"
)

const (
	UnrecognizeWord = 1
	CJKWord         = 2
	LatinWord       = 3
)

var WORD_MAX_LENGTH = [4]int{0, 1, 2, 3}

var reBlock = regexp.MustCompile(`[\s+——！，。？、~@#￥%……&*（）《》；：“”‘’]+`)
var reLatin = regexp.MustCompile(`[a-zA-z0-9]+`)

var St Segmentationizer

func init() {
	fmt.Println("package init")
	St := new(Segmentationizer)
	St.initialize()
}

type Word struct {
	word  string
	freq  int
	wtype int
}

type Chunk struct {
	words []*Word
}

type Segmentationizer struct {
	initialized bool
	word_dict   map[string]*Word
	curPos      int
	curBlock    string
	curBlockLen int
	text        string
}

func (s *Segmentationizer) initialize() {
	log.Printf("Building dict...")
	if s.initialized {
		return
	}
	s.word_dict = make(map[string]*Word)
	//t1 = time.Now()
	log.Printf("Load dict")
	file, _ := os.Open("default.dic")
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		splited := strings.Split(scanner.Text(), " ")
		word := splited[0]
		freq, _ := strconv.Atoi(splited[1])
		s.word_dict[word] = &Word{word, freq, CJKWord}

	}
	s.initialized = true
}

func (s *Segmentationizer) reinit(text string) {
	if !s.initialized {
		s.initialize()
	}
	s.text = text
}

func (s *Segmentationizer) createBlocks(text string) []string {
	return reBlock.Split(text, -1)
}

func (s *Segmentationizer) segment(text string) []string {
	s.reinit(text)
	res := make([]string, 0)
	blocks := s.createBlocks(text)
	for _, block := range blocks {
		fmt.Println("new block", block)
		s.curBlock = block
		s.curPos = 0
		s.curBlockLen = len([]rune(block))
		for {
			token := s.nextToken()
			if token != nil {
				res = append(res, token.word)
			} else {
				break
			}
		}
	}
	return res
}

func (s *Segmentationizer) nextToken() *Word {
	if s.curPos >= s.curBlockLen {
		return nil
	}
	word := s.isLatinWord()
	if word != nil {
		return word
	}
	return s.isCJKWord()
}

func (s *Segmentationizer) isLatinWord() *Word {
	latin_from := s.curPos
	latin_to := s.curPos
	for latin_to < s.curBlockLen {
		ch := s.curBlock[latin_to]
		if isLatinCh(ch) {
			latin_to++
		} else {

			break
		}

	}

	if latin_to > latin_from {
		s.curPos = latin_to
		return &Word{s.curBlock[latin_from:latin_to], 0, LatinWord}
	}
	return nil
}

func (s *Segmentationizer) isCJKWord() *Word {
	chunks := s.getChunks()
	// for _, filter := range s.filters {
	// 	chunks = filter(chunks)
	// }

	s.curPos += len([]rune(chunks[0].words[0].word))
	return chunks[0].words[0]
}

func (s *Segmentationizer) getChunks() []*Chunk {
	chunks := make([]*Chunk, 0)
	word0Idx := s.curPos
	for _, word0 := range s.getWordFromDict(word0Idx) {
		word1Idx := word0Idx + len([]rune(word0.word))
		if word1Idx < s.curBlockLen {
			for _, word1 := range s.getWordFromDict(word1Idx) {
				word2Idx := word1Idx + len([]rune(word1.word))
				if word2Idx < s.curBlockLen {
					for _, word2 := range s.getWordFromDict(word2Idx) {
						if word2.wtype == UnrecognizeWord {
							chunks = append(chunks, &Chunk{[]*Word{word0, word1}})
						} else {
							chunks = append(chunks, &Chunk{[]*Word{word0, word1, word2}})
						}
					}
				} else {
					chunks = append(chunks, &Chunk{[]*Word{word0, word1}})
				}
			}
		} else {
			chunks = append(chunks, &Chunk{[]*Word{word0}})
		}
	}
	return chunks
}

func (s *Segmentationizer) getWordFromDict(wordPos int) []*Word {
	matchedWords := make([]*Word, 0)
	tmpBlock := []rune(s.curBlock)

	for _, i := range WORD_MAX_LENGTH {
		if wordPos+i == len(tmpBlock) || isLatinRune(tmpBlock[wordPos+i]) {
			break
		}
		tmpRune := tmpBlock[wordPos : wordPos+i+1]
		tmpWord := runesToString(tmpRune)
		if elem, in_dict := s.word_dict[tmpWord]; in_dict {
			matchedWords = append(matchedWords, elem)
		}
		if len(matchedWords) == 0 {
			matchedWords = append(matchedWords, &Word{tmpWord, 0, UnrecognizeWord})
		}
	}
	return matchedWords
}

func isLatinCh(ch byte) bool {
	return reLatin.MatchString(string(ch))
}

func isLatinRune(r rune) bool {
	if len(string(r)) == 1 {
		return isLatinCh(string(r)[0])
	}
	return false
}

func runesToString(runes []rune) string {
	res := make([]string, 10)
	for _, tmp := range runes {
		res = append(res, string(tmp))
	}
	return strings.Join(res, "")
}

func main() {
	text := "abcd，研究生命起源"
	fmt.Println("main: ", text)
	fmt.Println(St.segment(text))
}
